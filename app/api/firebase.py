# -- coding: utf-8 --
"""
Provides an abstraction layers between Flask models and Firebase through Pyrebase lib
"""

import os
import threading
from datetime import datetime
from threading import Timer
import json
import pyrebase
from flask import Flask, current_app

__author__ = "Mauricio Domingos dos Santos Junior"
__copyright__ = "Copyright 2017, SheetGo Test Project"
__credits__ = ["Mauricio Domingos dos Santos Junior"]
__license__ = "GPL"
__version__ = "1.0.1"
__maintainer__ = "Mauricio Domingos dos Santos Junior"
__email__ = "mauricio14junior@gmail.com"
__status__ = "Development"


app = Flask(__name__)
"""
Even creating an app instance Flask doesn't load the settings we define unless we are in a request 
context. Which is not the case when the project is running.

From:
http://flask.pocoo.org/docs/0.12/config/#instance-folders

For some reason app.instance_path only brings the right path if the app is created from a root 
based module. It seems that the Flask object gets the path from the app.py module and cutts-off the 
current package from the path so it can go back a directory. So if we instantiate the app from an 
inner module it will assume that the instance folder is one level up. Taking it under consideration 
I had to split the path to go one more level up, in order to have the default instance folder.
"""
# Right path to instance /<git-folder>/instance/
# Is at the same level of the app folder

wrong_path = app.instance_path # <path>/sheetgo-test/app/instance/config.py
split_path = wrong_path.split('/') # ['sheetgo-test', 'app', 'instance']
split_path = split_path[:-2] # ['sheetgo-test']
split_path.append('instance') # ['sheetgo-test', 'instance']
instance_path = '/'.join(split_path) # sheetgo-test/instance
app.instance_path = instance_path


app.config.from_object('config.default')
app.config.from_pyfile(os.path.join(instance_path, 'config.py'))

with app.app_context():
    try:
        config = app.config["FIREBASE_CONFIG"]
    except Exception:
        config = None
    
    class FirebaseHandler(object):
        """
        This module is an abstration layer for the pyrabase module
        DOCS.: https://github.com/thisbejim/Pyrebase
        """

        # Static attribute that can be overriden at the constructor.

        def __init__(self, config=config):
            self.config = config
            self.firebase = pyrebase.initialize_app(config)
            self.connect_user()


        def connect_user(self):
            email = self.config['authEmail']
            password = self.config['authPass']
            self.auth = self.firebase.auth()
            self.user = self.auth.sign_in_with_email_and_password(email, password)
            self.last_auth = datetime.now()

        def get_db(self):
            """ User is a type dict obj. For some reason <refresh> method doesn't return the whole
             user object, lacking some attributes as expiresIn. For that reason I decided to
             call update on it in order to merge the old values with the new one """
            if self.expired():
                self.connect_user()
            self.user.update(self.auth.refresh(self.user['refreshToken']))
            return self.firebase.database()

        def expired(self):
            """ Checks wether the firebase authentication token has expired """
            current_time = datetime.now()
            timediff = current_time - self.last_auth
            expires = int(self.user['expiresIn'])
            return timediff.seconds > expires


    FIREBASE = FirebaseHandler()


    class FirebaseModel(object):
        """
        Implements the methods used by it's inheriting classes to perform CRUD operations.
        """
        firebase = FIREBASE
        def __init__(self):
            self.id = None

        def save(self):
            """ The Idea is to improve it in the future """
            model_name = self.__class__.__name__.lower()
            data = vars(self).copy()
            del data['id']
            db = FirebaseModel.firebase.get_db()

            if not self.id:
                model = db.child(model_name.lower()).push(data, firebase.user['idToken'])
                self.id = model['name']
            else:
                db.child(model_name).child(self.id).update(data, firebase.user['idToken'])


        def delete(self):
            """ Deletes a model based on it's instance. """
            if self.id:
                db = FirebaseModel.firebase.get_db()
                model_name = self.__class__.__name__.lower()
                db.child(model_name).child(self.id).remove()


        @classmethod
        def deleteModel(cls, id):
            """ Deletes a model based on a given id.
            Args:
                cls (str): implied through @classmethod decorator
                id (str): id of the model set to be deleted.
            """
            db = FirebaseModel.firebase.get_db()
            model_name = cls.__name__.lower()
            db.child(model_name).child(id).remove()


        @classmethod
        def filter(cls, exact=False, **kwargs):
            """
            This method filters the model based on it's attribute. It returns a FirebaseModelList
            object
            Args:
                cls (str): implied through @classmethod decorator
                exact (boolean): tells the method to strictly compare the values on kwargs.

                Returns:
                    FirebaseModelList: a FirebaseModelList set of filtered models.
            """
            args = list(kwargs.items())
            if len(args) > 1 or len(args) < 1:
                raise Exception('Filter takes one argument: {0} given'.format(len(args)))

            model_name = cls.__name__.lower()
            attribute = args[0][0]
            value = args[0][1]


            db = FirebaseModel.firebase.get_db()
            db_output = db.child(model_name).get() # returns Firebase itterator with <each> method
            db_values = db_output.val() # return an OrderedDict Object
            if db_values:
                db_models = list(db_values.items()) # returns a simple list object
                if exact:
                    filtered_model = [dbm for dbm in db_models if dbm[1].get(attribute) == value]
                else:
                    filtered_model = [
                        dbm for dbm in db_models if value.lower() in dbm[1].get(attribute).lower()
                    ]
                return FirebaseModel.get_model_list(cls, filtered_model)
            return FirebaseModelList()


        @classmethod
        def all(cls):
            """Returns all the models from the database
            Args:
                cls (str): implied through @classmethod decorator

            Returns:
                FirebaseModelList: a FirebaseModelList set of models.
            """
            model_name = cls.__name__.lower()
            db = FirebaseModel.firebase.get_db()
            db_output = db.child(model_name).get() # returns Firebase itterator with <each> method
            db_values = db_output.val() # return an OrderedDict Object
            db_models = list(db_values.items()) # returns a simple list object
            return FirebaseModel.get_model_list(cls, db_models)


        @classmethod
        def get_by_id(cls, id):
            """
            This method returns the model based on it's id. It returns a FirebaseModel
            object
            Args:
                cls (str): implied through @classmethod decorator
                id (str): the record id on database.

                Returns:
                    FirebaseModel: a FirebaseModel instance corresponding to the id.
            """
            # Imports the module of which cls belongs to
            module = __import__(cls.__module__, fromlist=(cls.__name__))

            # Once we got the module we can have the class itself (not an instance yet).
            class_ = getattr(module, cls.__name__)

            model_name = cls.__name__.lower()
            db = FirebaseModel.firebase.get_db()

            db_output = db.child(model_name).child(id).get().val() # returns a OrderedDict
            if db_output:
                model_dict = dict(db_output)
                user = class_(**model_dict)
                user.id = id
                return user # :)
            return None # :'(


        @staticmethod
        def get_model_list(cls, db_models):
            """ This method transforms the list brought from Firebase to a  FirebaseModelList()
                Once it's a static method the idea is to dynamically instantiate the model
                inheriting this class by obaining it's module path and Class name.

                Args:
                    cls (str): The name of the model subclass inheriting FirebaseModel.
                    param2 (list): The objects requested from Firebase transformed into list type.

                Returns:
                    FirebaseModelList: a FirebaseModelList list containing a set of models.
                Note:
                    1) In order to dinamically create the models without having to create dependency
                    with the model inheriting this class, Firabasemodel class methods have to access
                    the inheriting class name.
                    2) Knowing that (1), note that, unlike the the static filter method, this
                    static method calls for the @staticmethod decorator. Unlike the @classmethod
                    decorator, this one doesn't have Class attr (cls) implied when called. When the
                    the @classmethod decorator is used, the name of the subclass inheriting
                    Firabasemodel class will be passed automatically to the cls parameter. This
                    method uses @staticmethod because it's being evoked only from within
                    Firabasemodel, thus not having access to the Subclass name.
            """
            # Imports the module of which cls belongs to
            module = __import__(cls.__module__, fromlist=(cls.__name__))

            # Once we got the module we can have the class itself (not an instance yet).
            class_ = getattr(module, cls.__name__)

            fb_model_list = FirebaseModelList()
            for model in db_models:
                # This is a dict object containing the attributes and values from the object brought
                # by the Firebase request. Which happens to be the exact same attributes from the
                # model :)
                constructor_args = model[1]
                # import pdb
                # pdb.set_trace()
                # Now to instantiate a new model we pass the dict as it's constructor args
                model_instance = class_(**constructor_args)
                model_instance.id = model[0]

                fb_model_list.append(model_instance)

            return fb_model_list

    class FirebaseModelList(list):
        def first(self):
            if len(self) > 0:
                return self[0]
            return None

        def excludes(self, **kwargs):
            new_list = self
            for arg, value in kwargs.items():
                try:
                    new_list = FirebaseModelList(
                        [x for x in new_list if not getattr(x, arg).lower() == value.lower()]
                    )
                except:
                    return self
            return new_list


if __name__ == '__main__':
    pass