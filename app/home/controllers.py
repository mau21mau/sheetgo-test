# -- coding: utf-8 --
import json
import flask
from flask import current_app
from app.user.models import User

home = flask.Blueprint('home', __name__)

@home.route('/')
def index():
    user = None
    users = []
    if flask.session.get('user', None):
        user_json = json.loads(flask.session.get('user', ''))
        user = User.filter(email=user_json.get('email')).first()
        if not user:
            return flask.redirect(flask.url_for('user.login'))
        users = User.all().excludes(email=user.email)
    return flask.render_template('index.html', user=user, users=users)
