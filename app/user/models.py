# -- coding: utf-8 --
import json
from app.api.firebase import FirebaseModel


class User(FirebaseModel):
    
    def __init__(self, name, email, phone=None, address=None, pic=None):
        self.name = name
        self.email = email
        self.phone = phone
        self.address = address
        self.pic = pic
        super().__init__()

    def __repr__(self):
        return '<User %r>' % self.name

    def toJSON(self):
        """ 
        Returns a JSON string from the current object 
        """
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)

