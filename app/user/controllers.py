# -- coding: utf-8 --
from oauth2client import client
import httplib2
from apiclient import discovery
import json
import flask
from flask import request
from .forms import RegisterForm
from .models import User

user = flask.Blueprint('user', __name__)


@user.route('/login', methods=["GET", "POST"])
def login():
    """ This is the first endpoint of the authentication process """

    # Check whether or not the user has already been through the oauth authorization screen.
    # If not, than he is redirected to the oauth process.
    if 'credentials' not in flask.session:
        return flask.redirect(flask.url_for('googleauth.oauth2callback'))

    credentials = client.OAuth2Credentials.from_json(
        flask.session['credentials']
    )

    # Even if the user had been through authorization screen once, it might have expired.
    # In that casekick him back there!
    if credentials.access_token_expired:
        # If the credentials are expired, get them .
        return flask.redirect(flask.url_for('googleauth.oauth2callback'))
    else:
        # Now that we've got the credentials to act on user's behalf, we can finally build the api
        # object to perform api calls.
        http_auth = credentials.authorize(httplib2.Http())
        # the api object itself.
        drive = discovery.build('drive', 'v2', http_auth)

        # runing the api calls.
        about = drive.about().get().execute()  # Get about properties of Drive object
        # Get profile properties from authorized used
        profile = about.get('user')

        # Check if the user is already registered, by querying it's e-mail to the database.
        user = User.filter(email = profile.get('emailAddress')).first()
        if not user:
            user = User(
                name = profile.get('displayName'),
                email = profile.get('emailAddress'),
                pic = profile.get('picture').get('url') if profile.get('picture') else ''
            )
            flask.session['user'] = user.toJSON()
            form = RegisterForm(request.form)
            if request.method == 'GET':
                form = RegisterForm(obj=user)
                return flask.render_template('user/register.html', form=form, user=user)
            if form.validate_on_submit():
                user = User(
                    name = form.name.data,
                    email = form.email.data,
                    phone = form.phone.data,
                    address = form.address.data,
                    pic = profile.get('picture').get('url') if profile.get('picture') else ''
                )
                user.save()
            else:
                return flask.render_template('user/register.html', form=form, user=user)
        flask.session['user'] = user.toJSON()
        return flask.redirect(flask.url_for('home.index'))


@user.route('/logout')
def logout():
    credentials = client.OAuth2Credentials.from_json(flask.session['credentials'])
    try:
        credentials.revoke(httplib2.Http())
    except:
        pass
    flask.session.clear()
    return flask.redirect(flask.url_for('home.index'))


@user.route('/update/<user_id>', methods=["GET", "POST"])
def update(user_id):
    user = User.get_by_id(user_id)
    form = RegisterForm(request.form)
    if request.method == 'GET':
        form = RegisterForm(obj=user)
        return flask.render_template('user/register.html', form=form, user=user)
    if form.validate_on_submit():
        user = User(
            name = form.name.data,
            email = form.email.data,
            phone = form.phone.data,
            address = form.address.data,
            pic = user.pic
        )
        user.id = user_id
        user.save()
    flask.session['user'] = user.toJSON()
    return flask.redirect(flask.url_for('home.index'))
