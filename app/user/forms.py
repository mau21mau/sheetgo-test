# -- coding: utf-8 --
from flask_wtf import Form
from wtforms import StringField, PasswordField
from wtforms.validators import Required, Email


class RegisterForm(Form):
    name = StringField('Name', validators=[Required()])
    email = StringField('Email', validators=[Required(), Email()])
    phone = StringField('Phone', validators=[Required()])
    address = StringField('Address', validators=[Required()])
