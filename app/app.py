# -- coding: utf-8 --
"""
Module containing the app initialization statements
"""

from flask import Flask
from .home.controllers import home
from .user.controllers import user
from .googleauth.controllers import googleauth

app = Flask(__name__, instance_relative_config=True)
app.register_blueprint(home)
app.register_blueprint(user)
app.register_blueprint(googleauth)

# Load the default configuration
app.config.from_object('config.default')

# Load the configuration from the instance folder
#app.config.from_pyfile('config.py')

try:
    # Load the file specified by the APP_CONFIG_FILE environment variable
    # Variables defined here will override those in the default configuration
    app.config.from_envvar('APP_CONFIG_FILE')
except:
    pass