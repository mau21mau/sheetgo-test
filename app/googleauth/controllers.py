import json
import flask
from oauth2client import client

googleauth = flask.Blueprint('googleauth', __name__)


@googleauth.route('/oauth2callback')
def oauth2callback():
    """ This controller handles the callback url for oauth """

    # create a flow object. It doesn't provide any api access itself, though it's used to go through
    # the steps required for access granting.
    flow = client.flow_from_clientsecrets(
        flask.current_app.config['GOOGLE_CLI_SCRETS'],
        scope=flask.current_app.config['GOOGLE_CLI_SCOPE'],
        redirect_uri=flask.url_for(
            'googleauth.oauth2callback', _external=True
        ),
    )
    flow.params['access_type'] = 'offline'
    flow.params['include_granted_scopes'] = 'true' # boolean must be string.

    # Check if the authorization code is not already in the url. Meaning that user hasn't seen
    # the authorization interface yet. If that's the case we should take him there by redirecting
    # him so he can provide us the authorization code.
    if 'code' not in flask.request.args:
        auth_uri = flow.step1_get_authorize_url() # first step of the flow. Almost there :)
        return flask.redirect(auth_uri)
    else:
        # Once we have the authorization code we use it to retrieve the credentials needed to
        # perform api calls on the user's behaf. Credentials are, the, set to session.
        auth_code = flask.request.args.get('code')
        credentials = flow.step2_exchange(auth_code)
        flask.session['credentials'] = credentials.to_json()

        # Going back to the login :)
        return flask.redirect(flask.url_for('user.login'))
