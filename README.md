# Project initial requirements:
* [x] Web app coded in Python using either Flask or Django.
* [X] The app should be deployed on Google App Engine.
* [x] Using a no-sql solution among Parse, Firebase or Google Data Store.
* [x] The app should provide a simple User registry containing the fields: name, e-mail, phone and address.
* [x] And finally, the authentication method is oauth through Google OAuth.

Despite having more experience with Django, I decided to use Flask since the main advantage of Django is its powerful ORM system which isn't quite designed for no-sql databases, thus, unecessary for such scenario. I wanted to take a closer look into Flask, and since SheetGo uses it as its backend solution, it makes sense keeping it close to the company current stack.

## Setup the project.

I decided to use Python3.5 once 2.7 life is on countdown. __[EOL scheduled for 2020](https://pythonclock.org)__

Clone the project:
> git clone https://gitlab.com/mau21mau/sheetgo-test.git

Installing the virtuaenvwrapper
> sudo pip install virtualenvwrapper

Note: If you don't want to install it with sudo, you may run `pip install --user virtualenvwrapper`, though
it will install the env in a different location depending on your OS (usually ~/.local). Checkout the [docs](http://virtualenvwrapper.readthedocs.io/en/latest/install.html#basic-installation).

Open your `~/.bashrc` or your `~/.profile` or `~/.zshrc` and paste the following lines at the end of the file:
> nano ~/.bashrc
```
export WORKON_HOME=$HOME/.virtualenvs
export PROJECT_HOME=$HOME/Devel
source /usr/local/bin/virtualenvwrapper.sh
# If you installed it with pip install --user virtualenvwrapper source should propably be:
# source ~/.local/bin/virtualenvwrapper.sh
```

Create the virtualenv:
> mkvirtualenv sheetgo

If it doesn't enter the env after the creation simply run:
> workon sheetgo

Now install the python dependencies for the project:
> pip3 install -r requirements.txt


By default, the project will assume it's running at the development mode. If you want
to run it on a different one, set the a environment variable for the app workspace config file. The
variable is called `APP_CONFIG_FILE`, and should be one of the following options: 
`development.py`, `production.py` or `staging.py`. Example:

> export APP_CONFIG_FILE=/absolute-path/sheetgo-test/config/development.py

__Note:__ in order to run this project, you will have to replace the .demo files inside the `instance`
folder by renaming it to its real extension and providing their variables valid values.

Finally run the project:
> python3.5 run.py

# Firabase
## Docs
The module `app.api.firebase` contains 3 classes: `FirebaseHandler`, `FirebaeModel` and `FirebaseModelList`.

### FirebaseHandler

Is responsible for direct contact with the pyrebase library, which methods are
helpers for a better handling of api calls on Firebase. It supports only login/password user authentication
method due to a lack of support from pyrebase plugin.

By the default, it will set its `config` attribute from a global variable defined on the module
which loads the config from `instance/config.py`. But if necessary, It's constructor may receive a 
`config` argument which is a dictionary containing these values:
```
config = {
    "apiKey": "",
    "authDomain": "",
    "databaseURL": "",
    "projectId": "",
    "storageBucket": "projectId.appspot.com",
    "messagingSenderId": "",
    "serviceAccount": "",
    "authEmail": "",
    "authPass": ""
}
```

The `authEmail` and `authPass` is the user created at the Firebase pannel. Go to: `Authentication` > 
`Login methods` and enable E-mail/Password authentication. Then Go to `Users` tab and add a new user
with an e-mail and password which will be used on the config above.

#### Save some data to Firebase using the `push` method:
```
from app.api.firebase import FirebaseHandler
firebase = FirebaseHandler(config)
db = firebase.get_db()
data = {
    'title': 'Episode V: The Empire Strikes Back', 
    'year': '1980',
    'director': 'George Lucas',
    'protagonist': 'Mark Hamill'
}

# The push method saves data to the specified path 'movies', and returns the id of the object
db.child('movies').push(data, firebase.user['idToken']) # {'name': '-Kus55XvfSGjhnpvhBXO'}
``` 

#### Retrieve some data from Firebase:

*the method get returns an firebase itterable through which we can itterate with the `.each()` method*
```
movies = db.child('movies').get()
for movie in movies.each():
    print(movie.key()) # '-Kus55XvfSGjhnpvhBXO'
    print(movie.val()) # '{'protagonist': 'Mark Hamill', 'director': 'George Lucas', 'title': 'Episode V: The Empire Strikes Back', 'year': '1980'}'
```

#### Update some data from Firebase with the `update` method:

```
movie_pyresponse = db.child('movies').child('-Kus55XvfSGjhnpvhBXO').get()
movie.update(dict(movie_pyresponse.val())) # movie_pyresponse.val() returns an OrderedDict
movie['protagonist'] = 'Chewie <3'

db.child('movies').child('-Kus55XvfSGjhnpvhBXO').update(movie, firebase.user['idToken'])
```

#### Delete some data from Firebase with the `remove` method:
`db.child('movies').child('-Kus55XvfSGjhnpvhBXO').remove()`

### FirebaseModel

Works as DAO system (Data Access Object), providing methods for CRUD operations
of the the models inheriting it.

#### The `save` method saves a new instance or updates an existing one by checking the id attribute:

Let's suppose we have the following `user` model:


```
import json
from app.api.firebase import FirebaseModel


class User(FirebaseModel):
    def __init__(self, name, email, phone=None, address=None, pic=None):
        self.name = name
        self.email = email
        self.phone = phone
        self.address = address
        self.pic = pic
        super().__init__()
    def __repr__(self):
        return '<User %r>' % self.name
```

**Save the model**

```
user = User(
    'Spock', 
    'spock.vulcano@ussenterprise.com', 
    '+55 47-999245567', 
    'Vulcano Planet',
    'https://upload.wikimedia.org/wikipedia/pt/thumb/3/33/SpockNimoy.jpg/210px-SpockNimoy.jpg'
)

user.save() # Set the returned id from firebase
user.name = 'Mr. Spock'
user.save()
user.id # > '-KuvXSexNUPNo9ui_QLa'
```

**To retrieve a model from firebase use the method `get_by_id`**
```
user = User.get_by_id('-KuvXSexNUPNo9ui_QLa')
```

**We can add a model using an equivalent structured dictionary**
```
user_dict = {
    'name': 'James T. Kirk', 
    'email': 'kirk.human@ussenterprise.com', 
    'phone': '+55 47-992346852', 
    'address': 'Planet Earth',
    'pic': 'https://pt.wikipedia.org/wiki/Ficheiro:JamesTKirk.jpg'
}

user2 = User(**user_dict)
user2.save()
```

**It's also possible to retrieve multiple models by using `filter` or `all` methods** 

> *note that filter is performed programatically once Firebase doesn't support complex querying*

```
users = User.filter(name='james') # FirebaseModelList[<User 'James T. Kirk'>]
users = User.all() # FirebaseModelList[<User 'Spock'>, <User 'James T. Kirk'>]
```

**Since the list brought by these methods is a FirebaseModelList we can perform some custo actions 
such as `first()` which get's the first object from the list or `excludes(key=value)` which returns a list that
does not contain the matching key, value passed as parameter.**

```
users = User.all() # FirebaseModelList[<User 'Spock'>, <User 'James T. Kirk'>]
users.first() # <User 'Spock'>
users.excludes(email='kirk.human@ussenterprise.com') # FirebaseModelList[<User 'Spock'>]
users.first() # <User 'Spock'>
```