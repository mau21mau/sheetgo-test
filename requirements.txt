Flask==0.12.2
Flask-WTF==0.14.2
google-api-python-client==1.6.4
Pyrebase==3.0.27
oauth2client==3.0.0
gunicorn==19.7.1